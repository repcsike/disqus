# Changelog

## 2021-05-25 :: 0.4.0
* Works also as a webextension content script in Firefox
* Removes also top and sidebar advertisements
* Tested as Chrome, Firefox and Firefox Android extension

## 2021-05-25 :: 0.3.4
* Using reset also on recommendations, no more complaints from Disqus

## 2021-05-25 :: 0.3.3
* Not relying on window load event, instead checking for article every
  200 ms
* Using DISQUS.reset instead of loading embed.js after the first load

## 2021-05-24 :: 0.3.2
* Minor code improvements & fixed minor issues
* Moved globals into namespaces

## 2021-05-24 :: 0.3.1
* Fixed a bug: sometimes on phones, very rare cases on desktop, the
  buttons kept being added in an infinite cycle, resulting high CPU
  load and making it impossible to load the comments

## 2021-05-24 :: 0.3.0
* Another comments button in the authors line (page top)
* Using let and const instead of var
* Scrolling down upon loading comments
* More robust detection of article: checking every 200 ms for 6 s

## 2021-05-22 :: 0.2.5
* Included distribution URL in the header
* Loading comments if the URL points to a comment

## 2021-05-21 :: 0.2.4
* Adding CSS only once
* Green link colors in Disqus
* Reduced delay to 1000 ms
* Finds article also on blog subpages
* Removes some ads below articles

## 2021-05-21 :: 0.2.3
* Article not found if querySelector returns null
* Setting up the observer only once to avoid infinite loop,
  clearing interval in the beginning of the disqus444Main
* Adjusted class selector for article
* If fails to find article by class, falling back to parent of parent of
  #ap-article-footer1
* Added downloadURL = none, updateURL = none to the header
* Tried to use DISQUS.reset, but could not really make it work

## 2021-05-21 :: 0.2.2
* Padding for comments container: better readability on phones

## 2021-05-21 :: 0.2.1
* Added observer to reload comments when navigating to new URL

## 2021-05-21 :: 0.2.0
* Adjusted to new design
* Added 2000 ms delay, otherwise it loads prematurely
* Removed "highlight Czinkoczi"

## 2019-12-31 :: 0.1.3
* Added features from Gazsify444: highlight Czinkoczi and reddit button

## 2019-12-29 :: 0.1.2
* Comment button is always right after the article (above ads)
* Even if comments are allowed by the editors we create a new button
* Subdomains added to `@include` (e.g. kepek.444.hu)

## 2019-12-28 :: 0.1.1
* ?

## 2019-12-28 :: 0.1.0
* Comments button works: gets appended below all articles and loads
  comments on click