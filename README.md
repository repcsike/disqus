# Disqus fórum beágyazása a 444.hu cikkei alatt

![Screenshot of the page with the extension](/screenshots/green_buttons.png)

## Mit tud?

* A 444.hu oldalain megjelenik egy "Hozzászólások" gomb a cikkek alatt,
  ahogy ez régen minden cikknél alapból ott volt
* A gombra kattintva az oldalon belül betöltődnek a Disqus kommentek
* Működik minden aloldalon (pl. jo.444.hu) és a blogokon (pl.
  drogriporter.444.hu)
* A szerzők neve mellett is elhelyez egy gombot, így egy kattintással
  a kommentekre lehet ugrani
* A cikk alján megjelenik egy kék gomb is, amivel a Redditen nyithatsz
  egy topikot az adott cikknek
* A cikkek alatti reklámokat eltávolítja

## Telepítés

### Böngésző kiegészítőként

A Firefox kiegészítő nemsokára elérhető lesz a Mozilla oldalán:
https://addons.mozilla.org/en-US/firefox/addon/444-hu-disqus-embed/

Az xpi (Firefox) és crx (Chrome) csomagokat megtalálod a kiadásokhoz
csatolva is: https://gitlab.com/444hu/disqus/-/releases. Így tudod őket
telepíteni:

#### Firefox

1. Nyisd meg az `about:addons` oldalt.
2. Jobbra fenn, a kis fogaskerék menüben válaszd az Install addon from file
   pontot (lehet, hogy ez csak a Developer/Nightly Firefoxban működik?).
3. Válaszd ki a gitlabról letöltött xpi fájlt.

#### Chrome

1. Töltsd le a https://gitlab.com/444hu/disqus/-/releases oldalról a
   legfrissebb kiadásnál a `chrome.zip` fájlt (nem a sima zip fölül,
   hanem az alsó sorban a chrome.zip).
2. Csomagold ki a zip-et egy tetszőleges, üres könyvtárba.
3. Nyisd meg a `chrome://extensions` oldalt.
4. Jobbra fenn engedélyezd a Developer mode-ot.
5. Balra fenn válaszd a Load unpacked gombot és nyisd meg a könyvtárat,
   ahová a zip-et kicsomagoltad.

### Userscriptként (Greasemonkey, Tampermonkey)

1) Telepítsd a greasemonkey kiegészítőt vagy valamelyik alternatíváját
(tampermonkey, violentmonkey) innen:
https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
vagy innen:
https://www.tampermonkey.net/

2) Nyisd meg a greasemonkey menüjét (majom fej a jobb felső sarokban)
és válaszd az új user script opciót

3) Ekkor megnyílik egy szerkesztő ablak ahová másold be ennek a fájlnak a
tartalmát: https://gitlab.com/444hu/disqus/-/raw/master/js/444hu.disqus.user.js
és mentsd el.

#### Asztali gép

Asztali gépen Firefox + Greasemonkey és Chrome + Tampermonkey alatt
teszteltem, az előbbi környezetben többet. A kiegészítőket innen tudod
telepíteni:

* Firefox Greasemonkey: https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
* Firefox Tamermonkey: https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/
* Chrome Tampermonkey: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en

#### Mobil böngészők

Androidon Firefox + Tampermonkey környezetben használom a scriptet és
többnyire kifogástalanul működik. A telepítés azonban kissé nehézkes:

1. Először is, a Firefox alapból nem engedi telepíteni a Tampermonkey-t.
   Ezért regisztrálj a https://addons.mozilla.org/ oldalon.
2. Hozz létre egy collection-t. Ez egy privát gyűjteménye a kedvenc
   kiegészítőidnek.
3. Add hozzá ehhez a gyűjteményhez a Tampermonkey-t.
4. A telefonon a Firefox-ban (nem biztos, de lehet, hogy ez csak az ún.
   Nightly, azaz fejlesztői Firefox appal működik) a Settings-ben az
   About Firefox Nightly pont alatt nyomj rá 5-ször a logóra, míg a
   "Debug menu enabled" felirat megjelenik.
5. Ekkor a Settings-ben elérhető lesz a Custom Add-on Collection menüpont,
   itt írd be a saját gyűjteményed azonosítóját.
6. Ezután a Tampermonkey ajánlott kiegészítőként jelenik majd meg az Add-ons
   menüben, és egy kattintással telepítheted.
7. A Tampermonkey vezérlő felületét megnyitva, hozz létre egy új scriptet.
8. Ennek a tartalmát először töröld ki, majd másold be innen:
   https://openuserjs.org/src/scripts/amazing_value/444.hu_Disqus_Embed.user.js#bypass=true
   vagy innen: https://gitlab.com/444hu/disqus/-/raw/master/444hu.disqus.user.js
   a script tartalmát.
9. Végül kattints a Tampermonkey szerkesztőjében a File > Save menüre.

Innentől működnie kell. Sajnos néha a Firefox letiltja a Tampermonkey-t,
ilyenkor a böngésző újraindítása segít (Android Settings Apps menüjében
a Firefox-nál Force stop).

## Kérdés, hibajelentés, részvétel a fejlesztésben

Ha kérdésed, problémád van a script használatával kapcsolatban, vagy részt
vennél a fejlesztésben, keress meg az alábbi elérhetőségek bármelyikén.
A gitlab oldalon nyithatsz issue-t. Pull requesteket is szívesen fogadok,
vagy adhatok hozzáférést a repóhoz.

* https://gitlab.com/444hu/disqus/-/issues
* @amazingvalue (https://disqus.com/by/amazingvalue/)
* amazing_value@zohomail.eu

### Hibajelentés

Ha nem működik a cucc, vagy nem úgy működik ahogy kéne, különösen hasznos,
ha a böngésződ javascript konzolának üzeneteit mellékeled. Ehhez csupán
ennyit kell tenned:

1. A <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>J</kbd> billentyűkombinációval
   nyisd meg a browser console-t (nem a developer tools web konzolát!)
2. Navigálj a 444-re és nyiss meg egy cikket kommentekkel
3. A konzolon megjelenő listára jobb klikk, és "Export visible messages to"

## Frissítések

* https://openuserjs.org/scripts/amazing_value/444.hu_Disqus_Embed/
* https://gitlab.com/444hu/disqus

# Embed comments below all articles on 444.hu

## Updates

* https://openuserjs.org/scripts/amazing_value/444.hu_Disqus_Embed/
* https://gitlab.com/444hu/disqus

## Installation

### As a browser add-on

A Firefox add-on will be available soon by the Mozilla repository:
https://addons.mozilla.org/en-US/firefox/addon/444-hu-disqus-embed/

The xpi (Firefox) and crx (Chrome) packages are attached to the releases:
https://gitlab.com/444hu/disqus/-/releases.

#### Firefox

1. Navigate to `about:addons`.
2. From the top right cogwheel menu choose the Install addon from file point
   (not sure if that's only available in Developer/Nightly Firefox).
3. Open the xpi file downloaded from gitlab.

#### Chrome

1. From https://gitlab.com/444hu/disqus/-/releases download the latest
   `chrome.zip` bundle (make sure you choose the chrome.zip from the
   attachments on the bottom).
2. Extract the zip into an empty directory.
3. In Chrome, open the `chrome://extensions` page.
4. In the top right corner enable the Developer mode.
5. Top left, click on the Load unpacked button and select the directory
   you extracted the zip into.

### As a userscript

1) Install the "greasemonkey" add-on or some of the alternatives
(tampermonkey, violentmonkey) from
https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/ or
https://www.tampermonkey.net/

2) Open on the greasemonkey menu in your browser (monkey head near
top right corner) and click on "New user script"

3) Then an editor opens in another tab. Copy the content of the file
https://gitlab.com/444hu/disqus/-/raw/master/js/444hu.disqus.user.js into
into it and save.

#### Mobile browsers

Install Tampermonkey in Android Firefox Nightly this way:
https://blog.mozilla.org/addons/2020/09/29/expanded-extension-support-in-firefox-for-android-nightly/.
Copy paste the raw script from here: https://openuserjs.org/src/scripts/amazing_value/444.hu_Disqus_Embed.user.js#bypass=true
or here: https://gitlab.com/444hu/disqus/-/raw/master/444hu.disqus.user.js
into the Tampermonkey editor and save it. Force stop and restart Firefox
if does not work.

## Questions, feedback, contributions

With questions, issues or any feedback feel free to contact me by any of
the means listed below. Here on gitlab you can open issues. Pull requests
are also welcome or ask me for access to the repo.

* https://gitlab.com/444hu/disqus/-/issues
* @amazingvalue (https://disqus.com/by/amazingvalue/)
* amazing_value@zohomail.eu